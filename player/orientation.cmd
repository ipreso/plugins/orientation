#!/bin/bash
# Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
#
# This file is part of iPreso.
#
# iPreso is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any
# later version.
#
# iPreso is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General
# Public License along with iPreso. If not, see
# <https:#www.gnu.org/licenses/>.
#
# ====

SYNCHRO=/usr/bin/iSynchro
XRANDR=/usr/bin/xrandr
XRANDRALIGN=/usr/bin/xrandr-align
LOGO=/usr/share/plymouth/themes/ipresoblack/Splash-Black.png
LOGO_PORTRAIT=/var/lib/iplayer/cmd/Logo-portrait.png
LOGO_LANDSCAPE=/var/lib/iplayer/cmd/Logo-landscape.png
LOGO_LIBRARY=/var/lib/iplayer/library/Logo.png

if [ "$EUID" = "0" ] ; then
    su -c "$0 \"$@\"" player
    exit $?
fi

export DISPLAY=:0.0

# Wait until Xorg is running
MAX_WAIT=10
CURRENT_WAIT=0

xset q &>/dev/null
while [ "$?" != "0" ] ; do
    echo "Xorg is yet running, waiting..."
    if [ "${CURRENT_WAIT}" -gt "${MAX_WAIT}" ] ; then
        echo "Xorg is not running, exiting."
        exit 1
    fi
    CURRENT_WAIT=$((${CURRENT_WAIT} + 1))
    sleep 1
    xset q &>/dev/null
done

function usage ()
{
    echo "Usage:"
    echo "$0 <orientation>"
    echo ""
    echo "With <orientation> in:"
    echo "  -a      Apply last known configuration"
    echo "  -h      Display this usage screen"
    echo "  -i      Lanscape orientation (inverted)"
    echo "  -l      Portrait orientation (left)"
    echo "  -n      Lanscape orientation (normal)"
    echo "  -r      Portrait orientation (right)"
}

function setWP ()
{
        # Check Logo symbolic link                                                             
        if [ ! -h "$LOGO" ]; then                                                              
                                                                                               
            # Copy the original logo to the landscape logo                                     
            if [ -f "$LOGO" ]; then                                                            
                sudo cp $LOGO $LOGO.bak                                                             
                sudo mv $LOGO $LOGO_LANDSCAPE                                                       
            fi                                                                                 
                                                                                               
            # Create the symbolic link                                                         
            sudo ln -s $LOGO_LANDSCAPE $LOGO                                                        
            sudo chown -R player:player `dirname $LOGO`                                             
        fi                                                                   
}

function getMyFullPath ()
{
    LSOF=$(lsof -p $$ | grep -E "/"$(basename $0)"$")
    MY_PATH=$(echo $LSOF | sed -r s/'^([^\/]+)\/'/'\/'/1 2>/dev/null)
    echo $MY_PATH
}

function getMyConfFile ()
{
    DIR=`dirname $(getMyFullPath)`
    echo "$DIR/orientation.conf"
}

function parseConf ()
{
    CONFFILE=$(getMyConfFile)
    
    if [ ! -f $CONFFILE ];
    then
        return;
    fi

    CONFSCREEN=`grep ORIENTATION $CONFFILE | sed -r 's/^ORIENTATION=//g'`
}

function saveConf ()
{
    CONFFILE=$(getMyConfFile)

    echo "ORIENTATION=$CONFSCREEN" > $CONFFILE
}

function setOrientation ()
{
    # Get touchscreen device
    INPUT_DEVICE=$( ${XRANDRALIGN} list-input --short 2>&1 | \
                    grep "pointer" | grep "slave" | \
                    cut -c11- | \
                    sed -r 's/^(.*)id=.*$/\1/g' | \
                    sed -r 's/[\t ]+$//g' | \
                    tail -n1 )
    
    ROTATIONVALUE=$1
    ROTATIONOUTPUTS=$( $XRANDR | grep " connected" | cut -d' ' -f1 )
    for OUTPUT in ${ROTATIONOUTPUTS} ; do
        $XRANDR --output ${OUTPUT} --rotate ${ROTATIONVALUE}

        # Force the (last) output to be the primary output
        # Needed to correctly align the input device
        ${XRANDR} --output ${OUTPUT} --primary
    done

    ${XRANDRALIGN} align --input="${INPUT_DEVICE}"
}

function emissionReload ()
{
    echo "Reloading emission..."
    PROG=`iZoneMgr -l | grep "Program: " | cut -d"'" -f2`
    if [ ! -z "$PROG" ];
    then
        /usr/bin/iZoneMgr -c /etc/ipreso/iplayer.conf -p "$PROG"
    fi
}

function updateWallpaper ()
{
    # Link WallPaper picture to the splashy Logo
    if [ -e "$LOGO_LIBRARY" ];
    then
        rm -f $LOGO_LIBRARY
    fi
    ln -sf $LOGO $LOGO_LIBRARY

    # Create the wallpaper with the correct size
    RES=$( iWM -r )

    /usr/bin/convert $LOGO_LIBRARY -resize ${RES}! /var/lib/iplayer/library/${RES}_Logo.png
}

flag=
bflag=

parseConf

while getopts 'ahilnr' OPTION
do
  case $OPTION in
  a)    setWP
        if [ -n "$CONFSCREEN" ];
        then
            setOrientation $CONFSCREEN
        fi
        ;;
  h)    usage `basename $0`
        exit 0
        ;;
  i)
        CONFSCREEN="inverted"
        saveConf
        rm -f $LOGO && ln -s $LOGO_LANDSCAPE $LOGO
        setOrientation $CONFSCREEN
        updateWallpaper
        emissionReload
        ;;
  n)
        CONFSCREEN="normal"
        saveConf
        rm -f $LOGO && ln -s $LOGO_LANDSCAPE $LOGO
        setOrientation $CONFSCREEN
        updateWallpaper
        emissionReload
        ;;
  l)
        CONFSCREEN="left"
        saveConf
        rm -f $LOGO && ln -s $LOGO_PORTRAIT $LOGO
        setOrientation $CONFSCREEN
        updateWallpaper
        emissionReload
        ;;
  r)
        CONFSCREEN="right"
        saveConf
        rm -f $LOGO && ln -s $LOGO_PORTRAIT $LOGO
        setOrientation $CONFSCREEN
        updateWallpaper
        emissionReload
        ;;
  *)    usage `basename $0`
        exit 2
        ;;
  esac
done

exit 0
