<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
require_once 'Actions/Plugin/Skeleton.php';
require_once 'Actions/Plugin/OrientationTable.php';

define ("ORIENTATION_NORMAL",   0);
define ("ORIENTATION_RIGHT",    1);
define ("ORIENTATION_INVERT",   2);
define ("ORIENTATION_LEFT",     3);

class Actions_Plugin_Orientation extends Actions_Plugin_Skeleton
{
    public function __construct ()
    {
        $this->_name        = 'Orientation';
    }

    public function getName ()
    {
        return ($this->getTranslation ('Screen orientation'));
    }

    public function getContextProperties ($context)
    {
        return (NULL);
    }

    public function getPlayerCommand ($hash)
    {
        $toSend = array ();

        // Get the current emission of the box
        $tableOrientation   = new Actions_Plugin_OrientationTable ();
        $tableEmissions     = new Players_Programs ();
        $emission           = $tableEmissions->getCurrentEmission ($hash);

        if (!$emission)
            return (false);

        $layoutId           = $emission->getLayout ();
        $orientationBox     = $tableOrientation->get ("b-$hash");
        $orientationLayout  = $tableOrientation->get ("l-$layoutId");

        if ($orientationLayout !== false &&
            $orientationBox ['orientation'] != $orientationLayout ['orientation'])
        {
            // Box is not synced with its group !
            $tableOrientation->set ("b-$hash", $orientationLayout ['orientation']);
        }

        if ($orientationLayout === false && $orientationBox !== false &&
            $orientationBox ['orientation'] != ORIENTATION_NORMAL)
        {
            // Box is not synced with its group !
            $tableOrientation->set ("b-$hash", ORIENTATION_NORMAL);
        }

        $result = $tableOrientation->get ("b-$hash");
        if (!$result)
        {
            $tableOrientation->set ("b-$hash", $orientationLayout);
            $result = $tableOrientation->get ("b-$hash");
        }

        if ($result && $result ['modified'] == 1)
        {
            switch ($result ['orientation'])
            {
                case ORIENTATION_LEFT:
                    $params = "-l";
                    break;
                case ORIENTATION_RIGHT:
                    $params = "-r";
                    break;
                case ORIENTATION_INVERT:
                    $params = "-i";
                    break;
                case ORIENTATION_NORMAL:
                default:
                    $params = "-n";
            }

            $toSend = array (
                        'cmd'    => 'PLUGIN',
                        'params' => "orientation.cmd $params");

            $tableOrientation->setRead ("b-$hash");

            return ($toSend);
        }

        return (false);
    }

    public function getLayoutOrientation ($id)
    {
        $oTable = new Actions_Plugin_OrientationTable ();
        $result = $oTable->get ("l-$id");
        if ($result)
            $orientation = $result ['orientation'];
        else
            return ("l");

        if ($orientation == ORIENTATION_LEFT ||
            $orientation == ORIENTATION_RIGHT)
            return ("p");
        else
            return ("l");
    }

    public function setLayoutOrientation ($id, $value)
    {
        $oTable = new Actions_Plugin_OrientationTable ();
        switch ($value)
        {
            case "p":
                $orientation = ORIENTATION_LEFT;
                break;
            case "l":
            default:
                $orientation = ORIENTATION_NORMAL;
        }
        $oTable->set ("l-$id", $orientation);
    }
}
