CREATE TABLE IF NOT EXISTS Actions_orientation (
    `hash` varchar(100) NOT NULL,
    `orientation` int(2) DEFAULT 1,
    `modified` int(2) DEFAULT 1,
    PRIMARY KEY  (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

UPDATE Actions_orientation SET modified=1;
